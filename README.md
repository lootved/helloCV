# HelloCV

Basic "hello-world" OpenCV project to use as template.


# Neovim config
  `clang` LSP client will use `compile_commands.json` to resolve libraries includes.

  The following commands generates that file `bash build.sh -g`

# Setup
```bash
function rust_build_setup() {
	export CC=clang
	export CXX=${CC}
	export OPENCV_INCLUDE_PATHS=/usr/local/include/opencv4

	OPENCV_BUILD_DIR="$REPOS_DIR/opencv/build"
  export CMAKE_PREFIX_PATH=${OPENCV_BUILD_DIR}

	_prefix="${OPENCV_BUILD_DIR}/lib/libopencv_"
	export OPENCV_LINK_LIBS=$(echo ${_prefix}*.so | tr "[:space:]" ",")
}

function manual_build() {
	g++ src/main.cpp -o main -Wl,-rpath /usr/local/lib/libopencv_*
}
```

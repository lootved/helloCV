#!/bin/bash

[ "${1}" = "-f" ] && rm -rf out

# required by cmake to find CMakeFiles of OpenCV
OpenCV_DIR=${REPOS_DIR}/opencv/build
project="HelloCV"

mkdir -p out
cd out || exit 1
[ "${1}" = "-g" ] && {
	cmake -DOpenCV_DIR="${OpenCV_DIR}" \
		-DCMAKE_EXPORT_COMPILE_COMMANDS=1 ..
	mv compile_commands.json ../
	exit $?
}

[ -f build.ninja ] || cmake -DOpenCV_DIR="${OpenCV_DIR}" -GNinja ../

ninja

[ -z "${1}" ] || ./${project}

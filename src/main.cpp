#include <opencv2/opencv.hpp>

const char *window_name = "HelloCV";

void hello_world();
void show_webcam_feed();

int main(int argc, char **argv) {
  if (argc == 1) {
    hello_world();
  }
  show_webcam_feed();
  return 0;
}

void hello_world() {
  namedWindow(window_name, cv::WINDOW_AUTOSIZE);
  cv::Mat output = cv::Mat::zeros(120, 350, CV_8UC3);

  putText(output, "Hello World", cv::Point(15, 70), cv::FONT_HERSHEY_PLAIN, 3,
          cv::Scalar(125, 255, 30), 4);
  cv::imshow(window_name, output);
  cv::waitKey(0);
  cv::destroyWindow(window_name);
  exit(0);
}

void show_webcam_feed() {
  cv::VideoCapture cap(0);
  if (!cap.isOpened()) {
    std::cout << "ERROR INITIALIZING VIDEO CAPTURE" << std::endl;
    exit(1);
  }
  int stop = 0;
  while (!stop) {
    cv::Mat frame;

    if (!cap.grab()) {
      std::cout << "ERROR READING FRAME FROM CAMERA FEED" << std::endl;
      break;
    }

    cap.retrieve(frame);

    cv::Mat mat = cv::Mat(frame.rows, frame.cols, CV_8UC1);
    cv::imshow(window_name, frame);
#define ESCAPE_KEY 27
    switch (cv::waitKey(30)) {
    case ESCAPE_KEY:
      stop = 1;
      return;
    }
  }
  cv::destroyWindow(window_name);
}

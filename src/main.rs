use std::env;
use anyhow::Result;
use opencv::{
    prelude::*,
    videoio,
    highgui,
    imgproc::FONT_HERSHEY_PLAIN,
    imgproc::put_text,
    imgproc::LINE_8,
    core::Point,
    core::Scalar,
    core::CV_8UC3,

}; 

const ESC_CODE: i32 = 10;
const WINDOW_NAME :&str = "HelloCV";


fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() == 0 {
        hello_world()?;
    } else {
        show_webcam_feed()?;
    }
    Ok(())
}


fn hello_world() -> Result<()> {
    highgui::named_window(WINDOW_NAME, highgui::WINDOW_AUTOSIZE)?;
    let mut output= Mat::zeros(120, 350, CV_8UC3)?;
    let org:Point = Point::new(15, 70);
    let color:Scalar = Scalar::new(125.0, 255.0, 30.0, 0.0);
    unsafe {

    put_text(from_mat(output), "Hello World", org, FONT_HERSHEY_PLAIN, 3.0, color, 4, LINE_8, false);
    } 
    highgui::imshow(WINDOW_NAME, &output)?;
    highgui::wait_key(0);
    highgui::destroy_window(WINDOW_NAME);

    Ok(())
}

fn show_webcam_feed() -> Result<()> {
    highgui::named_window(WINDOW_NAME, highgui::WINDOW_AUTOSIZE)?;
    let mut cam = videoio::VideoCapture::new(0, videoio::CAP_ANY)?;
    let mut frame = Mat::default(); 
    loop {
        cam.read(&mut frame)?;
        highgui::imshow(WINDOW_NAME, &frame)?;
        let key = highgui::wait_key(1)?;
        if key == ESC_CODE {
            break;
        }
    }
    highgui::destroy_window(WINDOW_NAME);
    return Ok(())
}
